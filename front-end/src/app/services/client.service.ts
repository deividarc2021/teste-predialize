import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()
export class ClientService {
  private apiUrl = `${environment.api}/clients`;

  constructor(private http: HttpClient) {}

  getAll() {
    const clients = this.http.get(this.apiUrl);

    return clients;
  }

  getById(id) {
    const clients = this.http.get(`${this.apiUrl}/${id}`);

    return clients;
  }

  getByName(name) {
    const clients = this.http.get(`${this.apiUrl}/name/${name}`);

    return clients;
  }

  getGeneralTotals() {
    const totals = this.http.get(`${environment.api}/totals`);

    return totals;
  }

  getTotalsByCompany(id) {
    const totals = this.http.get(`${this.apiUrl}/${id}/totals`);

    return totals;
  }
}
