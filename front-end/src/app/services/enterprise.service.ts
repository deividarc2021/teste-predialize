import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()
export class EnterpriseService {
  private apiUrl = `${environment.api}/enterprises`;

  constructor(private http: HttpClient) {}

  getAll() {
    const enterprises = this.http.get(this.apiUrl);

    return enterprises;
  }

  getById(id) {
    const enterprises = this.http.get(`${this.apiUrl}/${id}/enterprise`);

    return enterprises;
  }

  getByName(name) {
    const enterprises = this.http.get(`${this.apiUrl}/name/${name}`);

    return enterprises;
  }

  getTotalsByEnterprise() {}

  getEnterprisesByCompany() {}
}
