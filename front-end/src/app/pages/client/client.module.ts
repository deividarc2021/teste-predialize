import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';

import { ClientComponent } from './client.component';
import { ClientService } from 'src/app/services/client.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { ClientDialogComponent } from '../clientdialog/clientdialog.component';

export const ClientRoutes: Routes = [
  {
    path: '',
    component: ClientComponent
  }
];

@NgModule({
  declarations: [
    ClientComponent,
    ClientDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    RouterModule.forChild(ClientRoutes)
  ],
  providers: [ClientService],
  entryComponents: [
    ClientDialogComponent
  ]
})
export class ClientModule {}
