import { Component, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { ClientService } from "src/app/services/client.service";
import { ClientDialogComponent } from "../clientdialog/clientdialog.component";

@Component({
  selector: "app-client",
  templateUrl: "./client.component.html",
  styleUrls: ["./client.component.scss"],
})
export class ClientComponent implements OnInit {
  clients: any[] = [];
  filter: string = '';

  totalClients: Number = 0;
  totalEnterprises: Number = 0;
  totalRealties: Number = 0;

  constructor(
    private clientService: ClientService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.clientService.getAll().subscribe(clients =>
      this.clients = clients as []
    );

    this.clientService.getGeneralTotals().subscribe((totals: any) => {
      this.totalClients = totals.clients;
      this.totalEnterprises = totals.enterprises;
      this.totalRealties = totals.realties;
    })
  }

  onChangeFilter() {
    this.filterByName(this.filter);
  }

  filterByName(name: string) {
    if (name.length >= 3) {
      this.clientService.getByName(name).subscribe(clients => {
        this.clients = clients as []
      })
    } else {
      this.clientService.getAll().subscribe(clients =>
        this.clients = clients as []
      );
    }
  }

  openDialog(id: number) {
    this.dialog.open(ClientDialogComponent, {
      data: { id }
    });
  }
}
