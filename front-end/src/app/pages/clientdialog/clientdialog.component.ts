import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ClientService } from 'src/app/services/client.service';

interface DialogData {
  id: number;
}

@Component({
  selector: 'app-clientdialog',
  templateUrl: './clientdialog.component.html',
  styleUrls: ['./clientdialog.component.scss']
})
export class ClientDialogComponent implements OnInit {
  client: any;
  totals: any;

  constructor(
    public dialogRef: MatDialogRef<ClientDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private clientService: ClientService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.clientService.getById(this.data.id).subscribe(client => {
      this.client = client;
    });

    this.clientService.getTotalsByCompany(this.data.id).subscribe(totals => {
      this.totals = totals;
    })
  }

  viewEnterprises(id) {
    this.router.navigate([`/enterprise/${id}`]);
    this.dialogRef.close();
  }

}
