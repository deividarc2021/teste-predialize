import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { EnterpriseService } from "src/app/services/enterprise.service";

@Component({
  selector: "app-enterprise",
  templateUrl: "./enterprise.component.html",
  styleUrls: ["./enterprise.component.scss"],
})
export class EnterpriseComponent implements OnInit {
  private id: string;
  enterprises: [] = [];

  constructor(
    private enterpriseService: EnterpriseService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    if (!!this.id) {
      this.enterpriseService.getById(this.id).subscribe((enterprisesData: []) => {
        this.enterprises = [];

        enterprisesData.map(enterprise => {
          this.enterprises.push(enterprise);
        })
      })
    } else {
      this.enterpriseService.getAll().subscribe((enterprisesData: []) => {
        this.enterprises = [];

        enterprisesData.map((enterprises: []) => {
          enterprises.map(enterprise => {
            this.enterprises.push(enterprise);
          })
        })
      })
    }
  }
}
