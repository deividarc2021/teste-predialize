import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

import { EnterpriseComponent } from './enterprise.component';
import { EnterpriseService } from 'src/app/services/enterprise.service';

export const EnterpriseRoutes: Routes = [
  {
    path: '',
    component: EnterpriseComponent,
  },
  {
    path: ':id',
    component: EnterpriseComponent,
  }
];

@NgModule({
  declarations: [EnterpriseComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    RouterModule.forChild(EnterpriseRoutes)
  ],
  providers: [EnterpriseService]
})
export class EnterpriseModule {}
