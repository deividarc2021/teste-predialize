module.exports = app => {
    const getTotals = id => {
        const client = app.database.find(client => client._id === id);

        const enterprises = client.enterprises.length;

        let realties = 0;

        client.enterprises.map(enterprise => {
            realties += Number(enterprise.realties);    
        });

        return { enterprises, realties }; 
    }

    return { getTotals }
}

