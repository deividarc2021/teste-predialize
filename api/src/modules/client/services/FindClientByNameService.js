module.exports = app => {
    const find = name => {
        const clients = app.database.filter(client => 
            client.name.toUpperCase().indexOf(name.toUpperCase()) !== -1
        );
        
        return clients;
    }

    return { find }
}

