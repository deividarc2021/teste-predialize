const ClientController = require('../controllers/ClientController');

module.exports.load = app => {
    /** Get all clients */
  app.get("/clients/", (req, res, next) => {
      const clients = ClientController(app).getAll();

      res.status(200).json(clients);
  });  

  /** Get clients by name */
  app.get("/clients/name/:name", (req, res, next) => {
    const name = req.params.name;

    const clients = ClientController(app).getByName(name);

    res.status(200).json(clients);
  });
  
  /** Get client totals */
  app.get("/clients/:client_id/totals", (req, res, next) => {
    const id = req.params.client_id;

    const totals = ClientController(app).getTotalById(id);

    res.status(200).json(totals);
  });

  /** Get a client by _id */
  app.get("/clients/:_id", (req, res, next) => {
    const id = req.params._id;

    const client = ClientController(app).getById(id);

    res.status(200).json(client);
  });
}