const FindClientByIdService = require('../services/FindClientByIdService');
const FindClientByNameService = require('../services/FindClientByNameService');
const FindClientTotalsService = require('../services/FindClientTotalsService');
const ListClientService = require('../services/ListClientSerivce');

module.exports = app => {
    const getAll = () => {
        const clientsData = ListClientService(app).list();

        const clients = clientsData.map(client => {
            const { _id, image_src, name } = client;
            const totals = getTotalById(_id);
            
            return { id: _id, image_src, name, totals } 
        })

        return clients;
    }

    const getByName = name => {
        const clientsData = FindClientByNameService(app).find(name);

        const clients = clientsData.map(client => {
            const { _id, image_src, name } = client;
            const totals = getTotalById(_id);
            
            return { id: _id, image_src, name, totals } 
        })

        return clients;
    }

    const getById = id => {
        return FindClientByIdService(app).find(id);
    }

    const getTotalById = id => {
        return FindClientTotalsService(app).getTotals(id);
    }


    return { getAll, getByName, getById, getTotalById }
}