const EnterpriseController = require("../controllers/EnterpriseController");


module.exports.load = app => {
  /** Get all enterprises */
  app.get("/enterprises", (req, res, next) => {
    const enterprises = EnterpriseController(app).getAll();

    return res.status(200).json(enterprises);
  });

  /** Get enterprises by name */
  app.get("/enterprises/name/:name", (req, res, next) => {
    const name = req.params.name;

    const enterprises = EnterpriseController(app).getByName(name);
    
    return res.status(200).json(enterprises);
  });

  /** Get all enterprises by client */
  app.get("/enterprises/:client_id/enterprise", (req, res, next) => {
    const id = req.params.client_id;

    const enterprises = EnterpriseController(app).getByClientId(id);
    
    return res.status(200).json(enterprises);
  });

  /** Get enterprises by client and name */
  app.get("/enterprises/:client_id/enterprise/name/:name", (req, res, next) => {
    const { client_id, name } = req.params;

    const enterprises = EnterpriseController(app).getByClientIdAndName(client_id, name);
    
    return res.status(200).json(enterprises);
  });
}