const ListEntrepriseService = require('../services/ListEntrepriseService');
const FindEnterpriseByClientIdService = require("../services/FindEnterpriseByClientIdService");
const FindEnterpriseByNameService = require("../services/FindEnterpriseByNameService");
const FindEnterpriseByClientIdAndNameService = require('../services/FindEnterpriseByClientIdAndNameService');

module.exports = app => {
    const getAll = () => {
        return ListEntrepriseService(app).list();        
    }

    const getByName = name => {
        return FindEnterpriseByNameService(app).find(name);
    }

    const getByClientId = id => {
        return FindEnterpriseByClientIdService(app).find(id);
    }

    const getByClientIdAndName = (id, name) => {
        return FindEnterpriseByClientIdAndNameService(app).find(id, name);
    }


    return { getAll, getByName, getByClientId, getByClientIdAndName }
}