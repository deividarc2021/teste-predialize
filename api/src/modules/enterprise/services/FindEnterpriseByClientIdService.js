const ListEntrepriseService = require("./ListEntrepriseService");

module.exports = app => {
    const find = id => {
        const clients = app.database;
               
        let enterprises = [];

        clients.map(client => {            
            if (client._id === id) {
                client.enterprises.map(enterprise => {
                    enterprises.push(enterprise);

                    return null;
                });
            }

            return null;
        });
        
        return enterprises;
    }

    return { find }
}

