const FindEnterpriseByClientIdService = require("./FindEnterpriseByClientIdService");

module.exports = app => {
    const find = (id, name) => {
        const enterprisesData = FindEnterpriseByClientIdService(app).find(id);

        const enterprises = enterprisesData.filter(enterprise => 
            enterprise.name.toUpperCase().indexOf(name.toUpperCase()) !== -1
        );
        
        return enterprises;
    }

    return { find }
}

