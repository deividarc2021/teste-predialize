const ListEntrepriseService = require("./ListEntrepriseService");

module.exports = app => {
    const find = name => {
        const enterprisesData = ListEntrepriseService(app).list();

        const enterprises = enterprisesData.filter(enterprise => 
            enterprise.name.toUpperCase().indexOf(name.toUpperCase()) !== -1
        );
        
        return enterprises;
    }

    return { find }
}

