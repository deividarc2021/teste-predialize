module.exports = app => {
    const list = () => {
        const clients = app.database;
                
        const enterprises = clients.map(client => {            
            return client.enterprises.map(enterprise => {
                return enterprise;
            });
        });

        return enterprises;
    }

    return { list }
}
