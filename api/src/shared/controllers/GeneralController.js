const FindGeneralTotalService = require('../services/FindGeneralTotalService');

module.exports = app => {

    const getTotals = () => {
        return FindGeneralTotalService(app).getTotals();
    }

    return { getTotals }
}