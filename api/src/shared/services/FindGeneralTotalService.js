module.exports = app => {
    const getTotals = () => {
        const clientsData = app.database;

        const clients = clientsData.length;
        let realties = 0;
        let enterprises = 0;

        clientsData.map(client => {
            enterprises += client.enterprises.length;

            client.enterprises.map(enterprise => {
                realties += Number(enterprise.realties);    
            });            
        });

        return { clients, enterprises, realties }; 
    }

    return { getTotals }
}

