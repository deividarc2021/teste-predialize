
const ClientRoutes = require('../../../modules/client/routes/client.routes');
const EnterpriseRoutes = require('../../../modules/enterprise/routes/enterprise.routes');
const GeneralController = require('../../controllers/GeneralController');

module.exports.load = (app) => {

  ClientRoutes.load(app);
  EnterpriseRoutes.load(app);

  /** Get general totals */
  app.get("/totals", (req, res, next) => {
    const totals = GeneralController(app).getTotals();

    return res.status(200).json(totals);
  });  
};
